# -*- coding: utf-8 -*-
from __future__ import print_function, division


import json
import requests


def load_json():
    api_token = "ada4c81264648f1225b06033589b3255"
    url = "http://ws.audioscrobbler.com/2.0/?method=geo.getTopArtists&api_key={}&country=Spain&format=json".format(api_token)
    data = requests.get(url).text
    data = json.loads(data)
    print (data['topartists']['artist'][0]['name'])


if __name__ == "__main__":
    data = load_json()
    print(json.dumps(data, indent=2, sort_keys=True))
