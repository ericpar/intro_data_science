import pandas

path = "./data/baseballdatabank-master/core/Master.csv"

baseball_data = pandas.read_csv(path)

print baseball_data['nameFirst']
baseball_data['height_plus_weight'] = baseball_data['height'] + baseball_data['weight']

output_path = "./data/baseball_modified.csv"
baseball_data.to_csv(output_path)

